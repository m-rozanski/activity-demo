Activity Demo
====================

It's a simple demo project for studies, just to familiarize with:

* Creating new activities
* Basic widgets
* [SharedPreferences][prefs]

[prefs]: https://developer.android.com/reference/android/content/SharedPreferences