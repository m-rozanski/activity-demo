package com.rozanski.swim_lab_1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.RatingBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), RatingBar.OnRatingBarChangeListener {
    private val PREF_NAME = "prefs"
    private val NAME = "name"
    private val EMPTY = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sharedPrefs: SharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txtWelcome.text = (txtWelcome.text.toString() + ",\n" + sharedPrefs.getString(NAME, EMPTY))

        btnFun.setOnClickListener {
            val act = Intent(this, FunActivity::class.java)
            startActivity(act)
        }

        btnLogout.setOnClickListener {
            val editor = sharedPrefs.edit()
            editor.remove(NAME)
            editor.apply()
            val act = Intent(this, LoginActivity::class.java)
            startActivity(act)
            finish()
        }

        ratingBar.onRatingBarChangeListener = this

    }

    override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {

        when (p1.toInt()) {
            0 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.sad_more))
            1 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.sad_med))
            2 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.sad))
            3 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.neutral))
            4 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.happy))
            5 -> imgFeel.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.happy_more))
        }

        Toast.makeText(this, R.string.made_by, Toast.LENGTH_SHORT).show()
    }

}
