package com.rozanski.swim_lab_1

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_fun.*

class FunActivity : AppCompatActivity() {

    private val MAX_ARGB_VAL = 255
    private var red: Int = 80
    private var green: Int = 180
    private var blue: Int = 80
    private var alpha: Int = MAX_ARGB_VAL

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fun)

        seekBarR.progress = red
        seekBarG.progress = green
        seekBarB.progress = blue
        seekBarA.progress = alpha
        txtRVal.text = red.toString()
        txtGVal.text = green.toString()
        txtBVal.text = blue.toString()
        txtAVal.text = alpha.toString()
        imgRefresh()

        img.setOnLongClickListener {
            val result: String =
                if (switchAlphaCanal.isChecked)
                    "ARGB: (" + alpha.toString() + ", " + red.toString() + ", " + green.toString() + ", " + blue.toString() + ")"
                else "RGB: (" + red.toString() + ", " + green.toString() + ", " + blue.toString() + ")"
            val msg = result + " " + getString(R.string.clipboard_copied)

            val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip: ClipData = ClipData.newPlainText("color", result)
            clipboardManager.primaryClip = clip

            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
            true
        }

        seekBarR.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                red = i
                imgRefresh()
                txtRVal.text = red.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        seekBarG.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                green = i
                imgRefresh()
                txtGVal.text = green.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        seekBarB.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                blue = i
                imgRefresh()
                txtBVal.text = blue.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        seekBarA.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                alpha = i
                imgRefresh()
                txtAVal.text = alpha.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        btnBack.setOnClickListener {
            finish()
        }

        switchAlphaCanal.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                layoutA.visibility = View.VISIBLE
            } else {
                layoutA.visibility = View.GONE
                alpha = MAX_ARGB_VAL
                seekBarA.progress = MAX_ARGB_VAL
                txtAVal.text = MAX_ARGB_VAL.toString()
            }
            imgRefresh()
        }

        Toast.makeText(this, R.string.clipboard_long_press, Toast.LENGTH_LONG).show()
    }

    private fun imgRefresh() {
        img.setBackgroundColor(Color.argb(alpha, red, green, blue))
    }
}

