package com.rozanski.swim_lab_1

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    private val PREF_NAME = "prefs"
    private val NAME = "name"
    private val EMPTY = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPrefs: SharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

        if (sharedPrefs.getString(NAME, EMPTY) != EMPTY) {
            log()
        } else {
            setContentView(R.layout.activity_login)

            checkBox.setOnCheckedChangeListener { _, isChecked ->
                btnLogin.isEnabled = isChecked
            }

            btnLogin.setOnClickListener {
                if (txtName.text == null || txtName.text.toString().isEmpty()) {
                    Toast.makeText(this, R.string.error_name, Toast.LENGTH_SHORT).show()
                } else if (txtEmail.text == null || !txtEmail.text.toString().contains('@')) {
                    Toast.makeText(this, R.string.error_email, Toast.LENGTH_SHORT).show()
                } else if (txtPassword.text == null || txtPassword.text.toString().length < 8) {
                    Toast.makeText(this, R.string.error_password, Toast.LENGTH_SHORT).show()
                } else if (!radioBtnFemale.isChecked && !radioBtnMale.isChecked) {
                    Toast.makeText(this, R.string.error_sex, Toast.LENGTH_SHORT).show()
                } else {
                    val editor = sharedPrefs.edit()
                    editor.putString(NAME, txtName.text.toString())
                    editor.apply()
                    log()
                }
            }

            radioBtnMale.setOnClickListener {
                if (radioBtnFemale.isChecked) radioBtnFemale.isChecked = false
            }

            radioBtnFemale.setOnClickListener {
                if (radioBtnMale.isChecked) radioBtnMale.isChecked = false
            }
        }
    }

    private fun log() {
        val act = Intent(this, MainActivity::class.java)
        startActivity(act)
        finish()
    }
}
